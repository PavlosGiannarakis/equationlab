//
//  PanClass.swift
//  EquationLab
//
//  Created by Giannarakis, Paul on 16/04/2016.
//  Copyright (c) 2016 Giannarakis, Paul. All rights reserved.
//

import UIKit

class PanClass {
    var TagEquation: Int? = nil
    var panEquation: UIPanGestureRecognizer!
    init(tag: Int, pan: UIPanGestureRecognizer )
    {
        self.TagEquation = tag
        self.panEquation = pan
    }
}
