//
//  ViewController.swift
//  EquationLab
//
//  Created by Giannarakis, Paul on 12/02/2016.
//  Copyright (c) 2016 Giannarakis, Paul. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: properties
    
    @IBOutlet weak var bordered_btn1: UIButton!
    @IBOutlet weak var bordered_btn2: UIButton!
    @IBOutlet weak var bordered_btn3: UIButton!
    @IBOutlet weak var bordered_btn4: UIButton!
    @IBOutlet weak var Hide_Equations: UIButton!
    @IBOutlet weak var Equations_view: UIView!
    @IBOutlet var panGesture: UIPanGestureRecognizer!
    @IBOutlet var panGesture2: UIPanGestureRecognizer!
    @IBOutlet var panGesture3: UIPanGestureRecognizer!
    @IBOutlet var panGesture4: UIPanGestureRecognizer!
    @IBOutlet var panGesture5: UIPanGestureRecognizer!
    @IBOutlet var tapGesture1: UITapGestureRecognizer!
    @IBOutlet var tapGesture2: UITapGestureRecognizer!
    @IBOutlet var tapGesture3: UITapGestureRecognizer!
    @IBOutlet var tapGesture4: UITapGestureRecognizer!
    @IBOutlet weak var Main_view: UIView!
    @IBOutlet weak var DynamicView: UIView!
    @IBOutlet var SuperView: UIView!
    @IBOutlet weak var Main_Label: UILabel!
    
    var Calculatorview: UIView!
    var scrollView: UIScrollView!
    var closeSlides: UIButton!
    @IBOutlet var slidesOnOff: UISwitch!
    
    var DataOf100 = Data()
    var DataOf101 = Data()
    var DataOf102 = Data()
    var DataOf103 = Data()
    
    var mainGreen = UIColor(red: CGFloat(136) / 255.0, green: CGFloat(200) / 255.0, blue: CGFloat(0) / 255.0, alpha: 1.0 )
    var clickedGreen = UIColor(red: CGFloat(99) / 255.0, green: CGFloat(145) / 255.0, blue: CGFloat(0) / 255.0, alpha: 1.0 )
    var MainGray = UIColor(red: CGFloat(153) / 255.0, green: CGFloat(153) / 255.0, blue: CGFloat(153) / 255.0, alpha: 1.0 )
    
    var isHidden: Bool? = nil
    
    struct defaultsKeys {
        static let keyOne = "firstStringKey"
        static let keyTwo = "secondStringKey"
        static let keyThree = "ThirdStringKey"
        static let keyFour = "FourthStringKey"
        
        static let posOneX = "firstPosKeyX"
        static let posOneY = "firstPosKeyY"
        
        static let posTwoX = "secondPosKeyX"
        static let posTwoY = "secondPosKeyY"
        
        static let posThreeX = "ThirdPosKeyX"
        static let posThreeY = "ThirdPosKeyY"
        
        static let posFourX = "FourthPosKeyX"
        static let posFourY = "FourthPosKeyY"
        
        static let lastFront = "LastFront"
        
        static let dataOneName = "dataOneKey"
        static let dataTwoName = "dataTwoKey"
        static let dataThreeName = "dataThreeKey"
        static let dataFourName = "dataFourKey"
        
        static let dataOneWeb = "dataOneKey1"
        static let dataTwoWeb = "dataTwoKey1"
        static let dataThreeWeb = "dataThreeKey1"
        static let dataFourWeb = "dataFourKey1"
        
        static let dataOneDescription = "dataOneKey2"
        static let dataTwoDescription = "dataTwoKey2"
        static let dataThreeDescription = "dataThreeKey2"
        static let dataFourDescription = "dataFourKey2"
        
        static let isHidden = "EquationView"
        
    }
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //slideshow()
        
        if let testOne = defaults.stringForKey(defaultsKeys.keyOne)
        {
            println("100 Does exist")
            //recontructData(100)
            createDynam(DataOf100)// use objects later on
            let positionX = defaults.floatForKey(defaultsKeys.posOneX)
            let positionY = defaults.floatForKey(defaultsKeys.posOneY)
            var Position: CGPoint!
            if positionX == 0 && positionY == 0
            {
                Position = CGPointMake(CGFloat(100),CGFloat(100))
            }
            else
            {
                Position = CGPointMake(CGFloat(positionX),CGFloat(positionY))
            }
            Main_view.viewWithTag(100)?.center = Position // can add option of point when creating to make life easier
        }
        else
        {
            println("100 Doesn't exist")
        }
        if let testtwo = defaults.stringForKey(defaultsKeys.keyTwo)
        {
            println("101 Does exist")
            //recontructData(101)
            createDynam(DataOf101)
            let positionX = defaults.floatForKey(defaultsKeys.posTwoX)
            let positionY = defaults.floatForKey(defaultsKeys.posTwoY)
            var Position: CGPoint!
            if positionX == 0 && positionY == 0
            {
                Position = CGPointMake(CGFloat(100),CGFloat(100))
            }
            else
            {
                Position = CGPointMake(CGFloat(positionX),CGFloat(positionY))
            }
            Main_view.viewWithTag(101)?.center = Position
        }
        else
        {
            println("101 Doesn't exist")
        }
        if let testThree = defaults.stringForKey(defaultsKeys.keyThree)
        {
            println("102 Does exist")
            //recontructData(102)
            createDynam(DataOf102)
            let positionX = defaults.floatForKey(defaultsKeys.posThreeX)
            let positionY = defaults.floatForKey(defaultsKeys.posThreeY)
            var Position: CGPoint!
            if positionX == 0 && positionY == 0
            {
                Position = CGPointMake(CGFloat(100),CGFloat(100))
            }
            else
            {
                Position = CGPointMake(CGFloat(positionX),CGFloat(positionY))
            }
            Main_view.viewWithTag(102)?.center = Position
        }
        else
        {
            println("102 Doesn't exist")
        }
        if let testFour = defaults.stringForKey(defaultsKeys.keyFour)
        {
            println("103 Does exist")
            //recontructData(103)
            createDynam(DataOf103)
            let positionX = defaults.floatForKey(defaultsKeys.posFourX)
            let positionY = defaults.floatForKey(defaultsKeys.posFourY)
            var Position: CGPoint!
            if positionX == 0 && positionY == 0
            {
                Position = CGPointMake(CGFloat(100),CGFloat(100))
            }
            else
            {
                Position = CGPointMake(CGFloat(positionX),CGFloat(positionY))
            }
            Main_view.viewWithTag(103)?.center = Position
        }
        else
        {
            println("103 Doesn't exist")
        }
        
        if defaults.valueForKey(defaultsKeys.lastFront) != nil
        {
            if Main_view.viewWithTag(defaults.integerForKey(defaultsKeys.lastFront))!.isDescendantOfView(Main_view)
            {
                gotFocus(defaults.integerForKey(defaultsKeys.lastFront))
            }
        }
        
        if defaults.valueForKey(defaultsKeys.isHidden) == nil
        {
            isHidden = false
            defaults.setBool(false, forKey: defaultsKeys.isHidden)
            println(".....")
            defaults.synchronize()
        }
        else if defaults.boolForKey(defaultsKeys.isHidden) == false
        {
            self.Equations_view.hidden = false
            isHidden = false
        }
        else if defaults.boolForKey(defaultsKeys.isHidden) == true
        {
            self.Equations_view.hidden = true
            isHidden = true
        }
        
        mainViewEmpty()
        
        bordered_btn1.layer.cornerRadius = 0
        bordered_btn1.layer.borderWidth = 1
        bordered_btn1.layer.borderColor = UIColor.whiteColor().CGColor
        
        bordered_btn2.layer.cornerRadius = 0
        bordered_btn2.layer.borderWidth = 1
        bordered_btn2.layer.borderColor = UIColor.whiteColor().CGColor
        
        bordered_btn3.layer.cornerRadius = 0
        bordered_btn3.layer.borderWidth = 1
        bordered_btn3.layer.borderColor = UIColor.whiteColor().CGColor
        
        bordered_btn4.layer.cornerRadius = 0
        bordered_btn4.layer.borderWidth = 1
        bordered_btn4.layer.borderColor = UIColor.whiteColor().CGColor
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    /*override func supportedInterfaceOrientations() -> Int {
        return Int(UIInterfaceOrientationMask.LandscapeLeft.rawValue)
    }*/
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setFrame(so: Bool)
    {
        if so == true
        {
            let newFrame = CGRect(x: 0, y: 95, width: 1024, height: 673)
            Main_view.frame = newFrame
            let newbtnFrame = CGRect(x: 0, y: 71, width: 33, height: 24)
            Hide_Equations.frame = newbtnFrame
            self.Hide_Equations.transform = CGAffineTransformMakeRotation((180*CGFloat(M_PI))/180)
        }
        else
        {
            let newFrame = CGRect(x: 220, y: 95, width: 804, height: 673)
            Main_view.frame = newFrame
            let newbtnFrame = CGRect(x: 186, y: 71, width: 33, height: 24)
            Hide_Equations.frame = newbtnFrame
            self.Hide_Equations.transform = CGAffineTransformMakeRotation(0)
        }
    }
    
    func mainViewEmpty()
    {
        println(Main_view.subviews.count)
        if Main_view.subviews.count == 1
        {
            Main_Label.hidden = false
        }
        else
        {
            Main_Label.hidden = true
        }

    }
    
    //Handle Taps on Dynamic views and gives them the focus
    @IBAction func handleTap(sender: UITapGestureRecognizer) {
        var viewToFront = sender.view!
        gotFocus(sender.view!.tag)
    }
    
    
    @IBAction func handleSecondTap(sender: UITapGestureRecognizer) {
        var viewToFront = sender.view!
        gotFocus(sender.view!.tag)
    }
    
    @IBAction func handleThirdTap(sender: UITapGestureRecognizer) {
        var viewToFront = sender.view!
        gotFocus(sender.view!.tag)
    }
    

    @IBAction func handleFourthTap(sender: UITapGestureRecognizer) {
        var viewToFront = sender.view!
        gotFocus(sender.view!.tag)
    }
    //End of Taps for dynamic views
    
    //Handle dragging gestures on the dynamic views
    @IBAction func handlePan(sender: UIPanGestureRecognizer) {
        var viewToMove = sender.view!
        move(viewToMove.tag)
    }
    
    @IBAction func handleSecondPan(sender: UIPanGestureRecognizer) {
        var viewToMove = sender.view!
        move(viewToMove.tag)
    }
    
    @IBAction func handleThirdPan(sender: UIPanGestureRecognizer) {
        var viewToMove = sender.view!
        move(viewToMove.tag)
    }
    
    @IBAction func handleFourthPan(sender: UIPanGestureRecognizer) {
        var viewToMove = sender.view!
        move(viewToMove.tag)
    }
    
    @IBAction func handleFifthPan(sender: UIPanGestureRecognizer) {
        var viewToMove = sender.view!
        move(viewToMove.tag)
    }
    //End of dragging handlers
    
    @IBAction func Hide_OnClick(sender: UIButton) {
        if Equations_view.hidden == false {
            Equations_view.hidden = true
            //let newFrame = CGRect(x: 0, y: 95, width: 1024, height: 673)
            //Main_view.frame = newFrame
            setFrame(true)
            defaults.setBool(true, forKey: defaultsKeys.isHidden)
            defaults.synchronize()
        }
        else
        {
            Equations_view.hidden = false
            setFrame(false)
            //let newFrame = CGRect(x: 220, y: 95, width: 804, height: 673)
            //Main_view.frame = newFrame
            defaults.setBool(false, forKey: defaultsKeys.isHidden)
            defaults.synchronize()
        }
    }

    //Button handlers
    @IBAction func SelectedOption(sender: AnyObject) {
        let button = sender as UIButton
        let ButtonTag = button.tag
        let sprViewTag = button.self.superview!.tag
        println(ButtonTag)
        println(sprViewTag)
        if ButtonTag > 4
        {
            controlDynam(ButtonTag, SuperVTagBtn: sprViewTag)
            println("Dynamic view altering")
        }
        else
        {
            setColor("\(button.tag)".toInt()!)
            println("Reseting color")
        }

    }
    
    //A function to move the dynamic views
    func move(Tag: Int)
    {
        var viewToMove = Main_view.viewWithTag(Tag)!
        if Tag != 104
        {
            gotFocus(viewToMove.tag)
        }
        let screenSize: CGRect = Main_view.bounds
        let Dynamicx = viewToMove.center.x
        let Dynamicy = viewToMove.center.y
        var width = (viewToMove.frame.width/2) - 1
        var height = (viewToMove.frame.height/2) - 1
        if viewToMove.tag == 100
        {
            viewToMove.center = panGesture.locationInView(Main_view)
            if panGesture.state == UIGestureRecognizerState.Ended
            {
                println("Gesture Ended")
            
                if (viewToMove.center.x - width) < Main_view.bounds.minX
                {
                    viewToMove.center.x = viewToMove.center.x + viewToMove.frame.width * 2
                    println("X repositioned")
                }
                if (viewToMove.center.y - height) < Main_view.bounds.minY
                {
                    viewToMove.center.y = viewToMove.center.y + viewToMove.frame.height * 2
                    println("Y repositioned")
                }
                if (viewToMove.center.x + width) > Main_view.bounds.maxX
                {
                    viewToMove.center.x = viewToMove.center.x - viewToMove.frame.width
                    println("X repositioned")
                }
                if (viewToMove.center.y + height) > Main_view.bounds.maxY
                {
                    viewToMove.center.y = viewToMove.center.y - viewToMove.frame.height
                    println("Y repositioned")
                }
                
                let Dynamicx2 = viewToMove.center.x
                let Dynamicy2 = viewToMove.center.y
                var array: [[CGFloat]] = [[Dynamicx2],[Dynamicy2]]
                println(array)
                defaults.setFloat(Float(Dynamicx2), forKey: defaultsKeys.posOneX)
                defaults.setFloat(Float(Dynamicy2), forKey: defaultsKeys.posOneY)
            }
        }
        else if viewToMove.tag == 101
        {
            viewToMove.center = panGesture2.locationInView(Main_view)
            if panGesture2.state == UIGestureRecognizerState.Ended
            {
                println("Gesture Ended")
                if (viewToMove.center.x - width) < Main_view.bounds.minX
                {
                    viewToMove.center.x = viewToMove.center.x + viewToMove.frame.width * 2
                    println("X repositioned")
                }
                if (viewToMove.center.y - height) < Main_view.bounds.minY
                {
                    viewToMove.center.y = viewToMove.center.y + viewToMove.frame.height * 2
                    println("Y repositioned")
                }
                if (viewToMove.center.x + width) > Main_view.bounds.maxX
                {
                    viewToMove.center.x = viewToMove.center.x - viewToMove.frame.width
                    println("X repositioned")
                }
                if (viewToMove.center.y + height) > Main_view.bounds.maxY
                {
                    viewToMove.center.y = viewToMove.center.y - viewToMove.frame.height
                    println("Y repositioned")
                }
                
                let Dynamicx2 = viewToMove.center.x
                let Dynamicy2 = viewToMove.center.y
                var array: [[CGFloat]] = [[Dynamicx2],[Dynamicy2]]
                println(array)
                defaults.setFloat(Float(Dynamicx2), forKey: defaultsKeys.posTwoX)
                defaults.setFloat(Float(Dynamicy2), forKey: defaultsKeys.posTwoY)
            }
        }
        else if viewToMove.tag == 102
        {
            viewToMove.center = panGesture3.locationInView(Main_view)
            if panGesture3.state == UIGestureRecognizerState.Ended
            {
                println("Gesture Ended")
                
                if (viewToMove.center.x - width) < Main_view.bounds.minX
                {
                    viewToMove.center.x = viewToMove.center.x + viewToMove.frame.width * 2
                    println("X repositioned")
                }
                if (viewToMove.center.y - height) < Main_view.bounds.minY
                {
                    viewToMove.center.y = viewToMove.center.y + viewToMove.frame.height * 2
                    println("Y repositioned")
                }
                if (viewToMove.center.x + width) > Main_view.bounds.maxX
                {
                    viewToMove.center.x = viewToMove.center.x - viewToMove.frame.width
                    println("X repositioned")
                }
                if (viewToMove.center.y + height) > Main_view.bounds.maxY
                {
                    viewToMove.center.y = viewToMove.center.y - viewToMove.frame.height
                    println("Y repositioned")
                }
                
                let Dynamicx2 = viewToMove.center.x
                let Dynamicy2 = viewToMove.center.y
                var array: [[CGFloat]] = [[Dynamicx2],[Dynamicy2]]
                println(array)
                defaults.setFloat(Float(Dynamicx2), forKey: defaultsKeys.posThreeX)
                defaults.setFloat(Float(Dynamicy2), forKey: defaultsKeys.posThreeY)
            }
        }
        else if viewToMove.tag == 103
        {
            viewToMove.center = panGesture4.locationInView(Main_view)
            if panGesture4.state == UIGestureRecognizerState.Ended
            {
                println("Gesture Ended")
                
                if (viewToMove.center.x - width) < Main_view.bounds.minX
                {
                    viewToMove.center.x = viewToMove.center.x + viewToMove.frame.width * 2
                    println("X repositioned")
                }
                if (viewToMove.center.y - height) < Main_view.bounds.minY
                {
                    viewToMove.center.y = viewToMove.center.y + viewToMove.frame.height * 2
                    println("Y repositioned")
                }
                if (viewToMove.center.x + width) > Main_view.bounds.maxX
                {
                    viewToMove.center.x = viewToMove.center.x - viewToMove.frame.width
                    println("X repositioned")
                }
                if (viewToMove.center.y + height) > Main_view.bounds.maxY
                {
                    viewToMove.center.y = viewToMove.center.y - viewToMove.frame.height
                    println("Y repositioned")
                }
                let Dynamicx2 = viewToMove.center.x
                let Dynamicy2 = viewToMove.center.y
                var array: [[CGFloat]] = [[Dynamicx2],[Dynamicy2]]
                println(array)
                defaults.setFloat(Float(Dynamicx2), forKey: defaultsKeys.posFourX)
                defaults.setFloat(Float(Dynamicy2), forKey: defaultsKeys.posFourY)
            }
        }
        else if viewToMove.tag == 104
        {
            viewToMove.center = panGesture5.locationInView(Main_view)
            if panGesture5.state == UIGestureRecognizerState.Ended
            {
                println("Gesture Ended")
                
                if (viewToMove.center.x - width) < Main_view.bounds.minX
                {
                    viewToMove.center.x = viewToMove.center.x + viewToMove.frame.width * 2
                    println("X repositioned")
                }
                if (viewToMove.center.y - height) < Main_view.bounds.minY
                {
                    viewToMove.center.y = viewToMove.center.y + viewToMove.frame.height * 2
                    println("Y repositioned")
                }
                if (viewToMove.center.x + width) > Main_view.bounds.maxX
                {
                    viewToMove.center.x = viewToMove.center.x - viewToMove.frame.width
                    println("X repositioned")
                }
                if (viewToMove.center.y + height) > Main_view.bounds.maxY
                {
                    viewToMove.center.y = viewToMove.center.y - viewToMove.frame.height
                    println("Y repositioned")
                }
            }
        }
        defaults.synchronize()
    }
    
    //A Function to give focus to tapped object
    func gotFocus(TagMoved: Int?)
    {
        if TagMoved == nil
        {
            whiteColor()
        }
        else
        {
            Main_view.bringSubviewToFront(Main_view.viewWithTag(TagMoved!)!)
            resetColor2()
            Main_view.viewWithTag(TagMoved!)?.backgroundColor = UIColor.whiteColor()
            Main_view.viewWithTag(TagMoved!)?.viewWithTag(TagMoved! + 100)?.backgroundColor = mainGreen
            defaults.setInteger(TagMoved!, forKey: defaultsKeys.lastFront)
            defaults.synchronize()
        }
    }
    
    //A function to control what goes on inside the dynamic views
    func controlDynam(ButtonTag: Int, SuperVTagBtn: Int)
    {
        if ButtonTag == 6
        {
            let ViewToClose = Main_view.viewWithTag(SuperVTagBtn)!
            let tagToRemove = ViewToClose.superview!.tag
            
            if let mainViewToClose = Main_view.viewWithTag(tagToRemove)
            {
                mainViewToClose.removeFromSuperview() // Removes defaults and dynamic view
                mainViewEmpty()
                if defaults.integerForKey(defaultsKeys.lastFront) == tagToRemove
                {
                    defaults.setValue(nil, forKey: defaultsKeys.lastFront)
                    gotFocus(nil)
                }
                if tagToRemove == 100
                {
                    defaults.setValue(nil, forKey: defaultsKeys.keyOne)
                    defaults.setValue(nil, forKey: defaultsKeys.posOneX)
                    defaults.setValue(nil, forKey: defaultsKeys.posOneY)
                    defaults.setValue(nil, forKey: defaultsKeys.dataOneWeb)
                    defaults.setValue(nil, forKey: defaultsKeys.dataOneName)
                    defaults.setValue(nil, forKey: defaultsKeys.dataOneDescription)
                    DataOf100 = Data()
                    //defaults.setValue(nil, forKey: defaultsKeys.dataOne)
                    defaults.synchronize()
                }
                if tagToRemove == 101
                {
                    defaults.setValue(nil, forKey: defaultsKeys.keyTwo)
                    defaults.setValue(nil, forKey: defaultsKeys.posTwoX)
                    defaults.setValue(nil, forKey: defaultsKeys.posTwoY)
                    defaults.setValue(nil, forKey: defaultsKeys.dataTwoWeb)
                    defaults.setValue(nil, forKey: defaultsKeys.dataTwoName)
                    defaults.setValue(nil, forKey: defaultsKeys.dataTwoDescription)
                    DataOf101 = Data()
                    //defaults.setValue(nil, forKey: defaultsKeys.dataTwo)
                    defaults.synchronize()
                }
                if tagToRemove == 102
                {
                    defaults.setValue(nil, forKey: defaultsKeys.keyThree)
                    defaults.setValue(nil, forKey: defaultsKeys.posThreeX)
                    defaults.setValue(nil, forKey: defaultsKeys.posThreeY)
                    defaults.setValue(nil, forKey: defaultsKeys.dataThreeWeb)
                    defaults.setValue(nil, forKey: defaultsKeys.dataThreeName)
                    defaults.setValue(nil, forKey: defaultsKeys.dataThreeDescription)
                    DataOf102 = Data()
                    //defaults.setValue(nil, forKey: defaultsKeys.dataThree)
                    defaults.synchronize()
                }
                if tagToRemove == 103
                {
                    defaults.setValue(nil, forKey: defaultsKeys.keyFour)
                    defaults.setValue(nil, forKey: defaultsKeys.posFourX)
                    defaults.setValue(nil, forKey: defaultsKeys.posFourY)
                    defaults.setValue(nil, forKey: defaultsKeys.dataFourWeb)
                    defaults.setValue(nil, forKey: defaultsKeys.dataFourName)
                    defaults.setValue(nil, forKey: defaultsKeys.dataFourDescription)
                    DataOf103 = Data()
                    //defaults.setValue(nil, forKey: defaultsKeys.dataFour)
                    defaults.synchronize()
                }
            }
        }
    }
    
    // A function that creates dynamic views
    func createDynam(data : Data)
    {
        if (Main_view.viewWithTag(100)?.isDescendantOfView(Main_view) == nil)
        {
            createView(100,data: data)
            setDataOfView(100, data : data)
        }
        else if (Main_view.viewWithTag(101)?.isDescendantOfView(Main_view) == nil)
        {
            createView(101,data: data)
            setDataOfView(101, data : data)
        }
        else if (Main_view.viewWithTag(102)?.isDescendantOfView(Main_view) == nil)
        {
            createView(102,data: data)
            setDataOfView(102, data : data)
        }
        else if (Main_view.viewWithTag(103)?.isDescendantOfView(Main_view) == nil)
        {
            createView(103,data: data)
            setDataOfView(103, data : data)
        }
        else
        {
            var alertView = UIAlertView()
            alertView.addButtonWithTitle("OK")
            alertView.title = "Oops"
            alertView.message = "Too many equations opened."
            alertView.show()
        }
        mainViewEmpty()
    }
    
    func setDataOfView(Tag: Int, data : Data)
    {
        if Tag == 100
        {
            defaults.setValue(data.getWebData(), forKey: defaultsKeys.dataOneWeb)
            defaults.setValue(data.getName(), forKey: defaultsKeys.dataOneName)
            defaults.setValue(data.getDescription(), forKey: defaultsKeys.dataOneDescription)
            defaults.synchronize()
        }
        else if Tag == 101
        {
            defaults.setValue(data.getWebData(), forKey: defaultsKeys.dataTwoWeb)
            defaults.setValue(data.getName(), forKey: defaultsKeys.dataTwoName)
            defaults.setValue(data.getDescription(), forKey: defaultsKeys.dataTwoDescription)
            defaults.synchronize()
        }
        else if Tag == 102
        {
            defaults.setValue(data.getWebData(), forKey: defaultsKeys.dataThreeWeb)
            defaults.setValue(data.getName(), forKey: defaultsKeys.dataThreeName)
            defaults.setValue(data.getDescription(), forKey: defaultsKeys.dataThreeDescription)
            defaults.synchronize()
        }
        else if Tag == 103
        {
            defaults.setValue(data.getWebData(), forKey: defaultsKeys.dataFourWeb)
            defaults.setValue(data.getName(), forKey: defaultsKeys.dataFourName)
            defaults.setValue(data.getDescription(), forKey: defaultsKeys.dataFourDescription)
            defaults.synchronize()
        }

    }
    
    func recontructData(Tag: Int)
    {
        if Tag == 100
        {
            DataOf100.setDescription(defaults.stringForKey(defaultsKeys.dataOneDescription)!)
            DataOf100.setName(defaults.stringForKey(defaultsKeys.dataOneName)!)
            DataOf100.setWebData(defaults.stringForKey(defaultsKeys.dataOneWeb)!)
        }
        else if Tag == 101
        {
            DataOf101.setDescription(defaults.stringForKey(defaultsKeys.dataTwoDescription)!)
            DataOf101.setName(defaults.stringForKey(defaultsKeys.dataTwoName)!)
            DataOf101.setWebData(defaults.stringForKey(defaultsKeys.dataTwoWeb)!)
        }
        else if Tag == 102
        {
            DataOf102.setDescription(defaults.stringForKey(defaultsKeys.dataThreeDescription)!)
            DataOf102.setName(defaults.stringForKey(defaultsKeys.dataThreeName)!)
            DataOf102.setWebData(defaults.stringForKey(defaultsKeys.dataThreeWeb)!)
        }
        else if Tag == 103
        {
            DataOf103.setDescription(defaults.stringForKey(defaultsKeys.dataFourDescription)!)
            DataOf103.setName(defaults.stringForKey(defaultsKeys.dataFourName)!)
            DataOf103.setWebData(defaults.stringForKey(defaultsKeys.dataFourWeb)!)
        }
    }
    
    //Set colors of buttons above table view
    func setColor(ButtonTag: Int)
    {
        if ButtonTag == 1
        {
            resetColor()
            bordered_btn1.backgroundColor = clickedGreen
        }
        else if ButtonTag == 2
        {
            resetColor()
            bordered_btn2.backgroundColor = clickedGreen
        }
        else if ButtonTag == 3
        {
            resetColor()
            bordered_btn3.backgroundColor = clickedGreen
        }
        else if ButtonTag == 4
        {
            resetColor()
            bordered_btn4.backgroundColor = clickedGreen
        }
    }
    
    func resetColor()
    {
        bordered_btn1.backgroundColor = mainGreen
        bordered_btn2.backgroundColor = mainGreen
        bordered_btn3.backgroundColor = mainGreen
        bordered_btn4.backgroundColor = mainGreen
    }
    //End of setting colours of buttons above table view
    
    //Helps reset and unset colours of the dynamic views when they lose or gain focus.
    func resetColor2()
    {
        if (Main_view.viewWithTag(100)?.isDescendantOfView(Main_view) != nil)
        {
            Main_view.viewWithTag(100)?.backgroundColor = UIColor.lightGrayColor()
            Main_view.viewWithTag(200)?.backgroundColor = MainGray
        }
        if (Main_view.viewWithTag(101)?.isDescendantOfView(Main_view) != nil)
        {
            Main_view.viewWithTag(101)?.backgroundColor = UIColor.lightGrayColor()
            Main_view.viewWithTag(201)?.backgroundColor = MainGray
        }
        if (Main_view.viewWithTag(102)?.isDescendantOfView(Main_view) != nil)
        {
            Main_view.viewWithTag(102)?.backgroundColor = UIColor.lightGrayColor()
            Main_view.viewWithTag(202)?.backgroundColor = MainGray
        }
        if (Main_view.viewWithTag(103)?.isDescendantOfView(Main_view) != nil)
        {
            Main_view.viewWithTag(103)?.backgroundColor = UIColor.lightGrayColor()
            Main_view.viewWithTag(203)?.backgroundColor = MainGray
        }
    }
    
    func whiteColor()
    {
        if (Main_view.viewWithTag(100)?.isDescendantOfView(Main_view) != nil)
        {
            Main_view.viewWithTag(100)?.backgroundColor = UIColor.whiteColor()
            Main_view.viewWithTag(100)?.viewWithTag(200)?.backgroundColor = mainGreen
        }
        if (Main_view.viewWithTag(101)?.isDescendantOfView(Main_view) != nil)
        {
            Main_view.viewWithTag(101)?.backgroundColor = UIColor.whiteColor()
            Main_view.viewWithTag(101)?.viewWithTag(201)?.backgroundColor = mainGreen
        }
        if (Main_view.viewWithTag(102)?.isDescendantOfView(Main_view) != nil)
        {
            Main_view.viewWithTag(102)?.backgroundColor = UIColor.whiteColor()
            Main_view.viewWithTag(102)?.viewWithTag(202)?.backgroundColor = mainGreen
        }
        if (Main_view.viewWithTag(103)?.isDescendantOfView(Main_view) != nil)
        {
            Main_view.viewWithTag(103)?.backgroundColor = UIColor.whiteColor()
            Main_view.viewWithTag(103)?.viewWithTag(203)?.backgroundColor = mainGreen
        }
    }
    
    //The Base function of creating views
    /*func infoBtn(sender: UIButton)
    {
    var alertView = UIAlertView()
    alertView.addButtonWithTitle("OK")
    alertView.title = "Oops"
    alertView.message = "Info of equation"
    alertView.show()
    }*/
    
    func createView(Tag: Int, data: Data)
    {
        var DynamicView=UIView(frame: CGRectMake(0, 0, 200, 200))
        DynamicView.backgroundColor=UIColor.whiteColor()
        DynamicView.layer.borderWidth=2
        DynamicView.tag = Tag
        println(Tag)
        if Tag == 100
        {
            DynamicView.addGestureRecognizer(panGesture)
            DynamicView.addGestureRecognizer(tapGesture1)
            defaults.setValue("100", forKey: defaultsKeys.keyOne)
            defaults.synchronize()
        }
        else if Tag == 101
        {
            DynamicView.addGestureRecognizer(panGesture2)
            DynamicView.addGestureRecognizer(tapGesture2)
            defaults.setValue("101", forKey: defaultsKeys.keyTwo)
            defaults.synchronize()
        }
        else if Tag == 102
        {
            DynamicView.addGestureRecognizer(panGesture3)
            DynamicView.addGestureRecognizer(tapGesture3)
            defaults.setValue("102", forKey: defaultsKeys.keyThree)
            defaults.synchronize()
        }
        else if Tag == 103
        {
            DynamicView.addGestureRecognizer(panGesture4)
            DynamicView.addGestureRecognizer(tapGesture4)
            defaults.setValue("103", forKey: defaultsKeys.keyFour)
            defaults.synchronize()
        }
        
        
        //SUBVIEW FOR TITLE, INFO, Close buttons
        
        var subDynamicView=UIView(frame: CGRectMake(0, 0, 200, 40))
        subDynamicView.backgroundColor = mainGreen
        subDynamicView.layer.cornerRadius = 0
        subDynamicView.layer.borderWidth = 1
        subDynamicView.tag = Tag + 100
        
        var subWebView = UIWebView(frame: CGRectMake(40,40,100,50))
        subWebView.loadHTMLString(data.getWebData(), baseURL: nil)
        subWebView.backgroundColor = UIColor.clearColor()
        subWebView.opaque = false
        subWebView.scrollView.scrollEnabled = false
        subWebView.scrollView.bounces = false
        
        
        //INFO Button
        var info_btn = UIButton.buttonWithType(UIButtonType.System) as UIButton
        info_btn.frame = CGRectMake(0,0,40,40)
        info_btn.layer.backgroundColor = UIColor.clearColor().CGColor
        info_btn.setTitle("Info",forState: UIControlState.Normal)
        info_btn.tag = 8
        info_btn.addTarget(self, action: "infoBtn:", forControlEvents: UIControlEvents.TouchUpInside)
        
        //TITLE
        var descLabel = UILabel(frame: CGRectMake(40, 0, 100, 40))
        descLabel.textColor = UIColor.whiteColor()
        descLabel.text = data.getName()
        descLabel.layer.backgroundColor = UIColor.clearColor().CGColor
        descLabel.textAlignment = NSTextAlignment.Center
        
        //Image of scholar hat (please find coordinates for image)
        /*var imageView : UIImageView
        imageView  = UIImageView(frame:CGRectMake(0, 0, 100, 200));
        imageView.image = UIImage(named:"scholarhat.png")
        imageView.layer.borderWidth=0*/
        
        
        //CLOSE button
        
        var close_btn = UIButton.buttonWithType(UIButtonType.System) as UIButton
        close_btn.frame = CGRectMake(140,0,60,40)
        close_btn.layer.borderWidth=0
        close_btn.layer.cornerRadius = 0
        close_btn.layer.backgroundColor = UIColor.clearColor().CGColor
        close_btn.setTitle("Close",forState: UIControlState.Normal)
        close_btn.titleLabel!.font =  UIFont(name:"Tahoma", size: 10)
        close_btn.tag = 6
        close_btn.addTarget(self, action: "SelectedOption:", forControlEvents: UIControlEvents.TouchUpInside)
        
        subDynamicView.addSubview(info_btn)
        subDynamicView.addSubview(close_btn)
        subDynamicView.addSubview(descLabel)
        //subDynamicView.addSubview(imageView)
        
        DynamicView.addSubview(subDynamicView)
        DynamicView.addSubview(subWebView)
        
        Main_view.addSubview(DynamicView)
        gotFocus(DynamicView.tag)
    }
    
    @IBAction func CreateCalculator(sender: UIButton) {//new
        let Calculatorview = Calculator(frame: CGRect(x: 0, y: 0, width: 220, height: 350))
        Calculatorview.layer.cornerRadius = 0
        Calculatorview.layer.borderWidth = 1
        Calculatorview.tag = 104
        Calculatorview.addGestureRecognizer(panGesture5)
        Main_view.addSubview(Calculatorview)
        mainViewEmpty()
    }
    
    func createWebCell(posCell: NSIndexPath, pos: CGPoint)//to simplify later on
    {
        if let tableView = self.childViewControllers[0] as? WebTableViewController
        {
            let webCell = tableView.TableView.cellForRowAtIndexPath(posCell)? as WebTableViewCell
            //var snapShot = ghostCell(webCell)
            //snapShot.addGestureRecognizer(extraPan)
            //SuperView.addSubview(webCell)
            //webCell.center = CGPointMake(0.0, 0.0)
            //extraPanHan(extraPan)
        }
    }
    
    func ghostCell(inputView: UIView) -> UIView {
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        
        inputView.layer.renderInContext(UIGraphicsGetCurrentContext())
        
        let image = UIGraphicsGetImageFromCurrentImageContext() as UIImage
        
        UIGraphicsEndImageContext()
        
        let cellSnapshot = UIImageView(image: image)
        
        cellSnapshot.layer.masksToBounds = false
        
        cellSnapshot.layer.cornerRadius = 0.0
        
        cellSnapshot.layer.shadowOffset = CGSizeMake(-2.0, 0.0)
        
        cellSnapshot.layer.shadowRadius = 5.0
        
        cellSnapshot.layer.shadowOpacity = 0.2
        
        var extraPan = UIPanGestureRecognizer(target: self, action: "extraPanHan:")
        
        cellSnapshot.addGestureRecognizer(extraPan)
        
        return cellSnapshot
        
    }
    
    
    func slideshow()
    {
        var width = SuperView.frame.width/2
        var height = SuperView.frame.height/2
        
        scrollView = UIScrollView()
        closeSlides  = UIButton()
        slidesOnOff = UISwitch()
        
        closeSlides.frame = CGRectMake(width * 1.55 , height * 0.5, 50, 25)
        closeSlides.backgroundColor = mainGreen
        closeSlides.layer.cornerRadius = 2;
        closeSlides.setTitle("Close", forState: UIControlState.Normal)
        closeSlides.addTarget(self, action: "closeSlides:", forControlEvents: UIControlEvents.TouchUpInside)
        scrollView.frame = CGRectMake(width/1.5, height/2, width, height)
        slidesOnOff.frame = CGRectMake(width * 1.55, height * 0.8 , 40, 30)
        
        let imgOne = UIImageView(frame: CGRectMake(0, 0,width, height))
        imgOne.image = UIImage(named: "slide1")
        let imgTwo = UIImageView(frame: CGRectMake(width, 0,width, height))
        imgTwo.image = UIImage(named: "slide2")
        let imgThree = UIImageView(frame: CGRectMake(width*2, 0,width, height))
        imgThree.image = UIImage(named: "slide3")
        let imgFour = UIImageView(frame: CGRectMake(width*3, 0,width, height))
        imgFour.image = UIImage(named: "slide4")
        let imgFive = UIImageView(frame: CGRectMake(width*4, 0,width, height))
        imgFive.image = UIImage(named: "slide5")
        let imgSix = UIImageView(frame: CGRectMake(width*5, 0,width, height))
        imgSix.image = UIImage(named: "slide6")
        let imgSeven = UIImageView(frame: CGRectMake(width*6, 0,width, height))
        imgSeven.image = UIImage(named: "slide7")
        let imgEight = UIImageView(frame: CGRectMake(width*7, 0,width, height))
        imgEight.image = UIImage(named: "slide8")
        
        
        scrollView.addSubview(imgOne)
        scrollView.addSubview(imgTwo)
        scrollView.addSubview(imgThree)
        scrollView.addSubview(imgFour)
        scrollView.addSubview(imgFive)
        scrollView.addSubview(imgSix)
        scrollView.addSubview(imgSeven)
        scrollView.addSubview(imgEight)
        scrollView.layer.borderWidth = 1.0
        
        view.addSubview(slidesOnOff)
        view.addSubview(scrollView)
        view.addSubview(closeSlides)
        
        scrollView.contentSize = CGSizeMake(width * 8, height)
        NSTimer.scheduledTimerWithTimeInterval(7, target: self, selector: "slidePages", userInfo: nil, repeats: true)
        
    }
    
    func slidePages (){
        
        let pageWidth:CGFloat = CGRectGetWidth(self.scrollView.frame)
        let maxWidth:CGFloat = pageWidth * 8
        let contentOffset:CGFloat = self.scrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        
        if  contentOffset + pageWidth == maxWidth{
            slideToX = 0
        }
        self.scrollView.scrollRectToVisible(CGRectMake(slideToX, 0, pageWidth, CGRectGetHeight(self.scrollView.frame)), animated: true)
    }
    
    func closeSlides(sender:UIButton!)
    {
        scrollView.removeFromSuperview()
        closeSlides.removeFromSuperview()
        slidesOnOff.removeFromSuperview()
    }
    
    @IBAction func saveSwitchState(sender: UISwitch) {
        
        var defaults = NSUserDefaults.standardUserDefaults()
        
        if slidesOnOff.on {
            defaults.setBool(true, forKey: "SwitchState")
            defaults.setBool(true, forKey: "onoroff")
            
        } else {
            defaults.setBool(false, forKey: "SwitchState")
            defaults.setBool(false, forKey: "onoroff")
            
        }
    }
}

