//
//  Data.swift
//  EquationLab
//
//  Created by Giannarakis, Paul on 06/04/2016.
//  Copyright (c) 2016 Giannarakis, Paul. All rights reserved.
//

import UIKit

class Data {
    
    private var equationName:String = ""
    private var WebViewDATA:String = ""
    private var equationDescription:String = ""
    private var Tag: Int = 0
    //var starred:Bool = false
    
    func setTag(var tag:Int)
    {
        Tag = tag
    }
    
    func setName(var name:String)
    {
        equationName = name
    }
    
    func setWebData(var data:String)
    {
        WebViewDATA = data
    }
    
    func setDescription(var description:String)
    {
        equationDescription = description
    }
    
    func getTag() -> Int
    {
        return Tag
    }
    
    func getName() -> String
    {
        return equationName
    }
    
    func getWebData() -> String
    {
        return WebViewDATA
    }
    
    func getDescription() -> String
    {
        return equationDescription
    }
    
}
