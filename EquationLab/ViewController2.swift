//
//  ViewController2.swift
//  EquationLab
//
//  Created by Giannarakis, Paul on 22/03/2016.
//  Copyright (c) 2016 Giannarakis, Paul. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {
    //MARK : Properties
    
    @IBOutlet var SecondSuperView: UIView!
    //@IBOutlet weak var DynamicView: UIView!
    //@IBOutlet weak var webViewDynam: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // if object exists 
        //load object
        //else do nothing
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func SelectedOption2(sender: AnyObject) {
        let button = sender as UIButton
        let ButtonTag2 = button.tag
        let sprViewTag = button.self.superview!.tag
        //println("\(button.tag)")
        if ButtonTag2 > 100
        {
        createView("\(button.tag)".toInt()!)
        }
        else
        {
        closeDynam(sprViewTag)
        }
    }
    

    func createView(ButtonTag: Int)
    {
        var DynamicView=UIView(frame: CGRectMake(0,0,800, 600))
        DynamicView.center.x = (SecondSuperView.frame.width/2) - 1
        DynamicView.center.y = (SecondSuperView.frame.height/2) - 1
        DynamicView.backgroundColor=UIColor.whiteColor()
        DynamicView.layer.cornerRadius=5
        DynamicView.layer.borderWidth=1
        DynamicView.tag = ButtonTag + 100
        var close_btn = UIButton.buttonWithType(UIButtonType.System) as UIButton
        close_btn.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        close_btn.frame = CGRectMake(700,0,100,20)
        close_btn.layer.cornerRadius = 5
        close_btn.layer.borderWidth=1
        close_btn.layer.borderColor = UIColor.lightGrayColor().CGColor
        close_btn.backgroundColor = UIColor(red: CGFloat(247) / 255.0, green: CGFloat(255) / 255.0, blue: CGFloat(230) / 255.0, alpha: 1.0)
        close_btn.setTitle("Close",forState: UIControlState.Normal)
        close_btn.tag = 16
        close_btn.addTarget(self, action: "SelectedOption2:", forControlEvents: UIControlEvents.TouchUpInside)
        DynamicView.addSubview(close_btn)
        var webViewDynam = UIWebView(frame: CGRectMake(5,20,795,565))
        webViewDynam.layer.backgroundColor = UIColor.clearColor().CGColor
        if ButtonTag == 201
        {
            let url = NSBundle.mainBundle().URLForResource("RearrangingEquations", withExtension:"html")
            let request = NSURLRequest(URL: url!)
            webViewDynam.loadRequest(request)
        }
        if ButtonTag == 202
        {
            let url = NSBundle.mainBundle().URLForResource("AnswerAccuracy", withExtension:"html")
            let request = NSURLRequest(URL: url!)
            webViewDynam.loadRequest(request)
        }
        if ButtonTag == 203
        {
            let url = NSBundle.mainBundle().URLForResource("SciNot", withExtension:"html")
            let request = NSURLRequest(URL: url!)
            webViewDynam.loadRequest(request)
        }
        if ButtonTag == 204
        {
            let url = NSBundle.mainBundle().URLForResource("Prefixes", withExtension:"html")
            let request = NSURLRequest(URL: url!)
            webViewDynam.loadRequest(request)
        }
        if ButtonTag == 205
        {
            let url = NSBundle.mainBundle().URLForResource("Databook", withExtension:"html")
            let request = NSURLRequest(URL: url!)
            webViewDynam.loadRequest(request)
        }
        DynamicView.addSubview(webViewDynam)
        SecondSuperView.addSubview(DynamicView)
    }
    
    func closeDynam(SuperViewTag: Int)
    {
        if let mainViewToClose = SecondSuperView.viewWithTag(SuperViewTag)
        {
            mainViewToClose.removeFromSuperview()
        }
    }

}
