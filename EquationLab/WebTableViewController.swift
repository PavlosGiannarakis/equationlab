//
//  WebTableViewController.swift
//  EquationLab
//
//  Created by Giannarakis, Paul on 06/04/2016.
//  Copyright (c) 2016 Giannarakis, Paul. All rights reserved.
//

import UIKit

class WebTableViewController: UITableViewController, NSXMLParserDelegate {
    
    //MARK: Properties
    
    var webData:String = ""
    var webDesription:String = ""
    var equationName:String = ""
    var variables:String = ""
    var equation:String = ""
    var parser = NSXMLParser()
    var data = [Data]()
    var paths = [Path]()
    var gestureArray = [PanClass]()
    var endedEquation = false
    var endedVariable = false
    var endedDescription = false
    var endedMath = false
    var full:String = ""
    var Position: CGPoint!
    @IBOutlet var TableView: UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadSampleData()
        TableView.separatorInset = UIEdgeInsetsZero
        TableView.contentInset =  UIEdgeInsetsZero
        TableView.backgroundColor = UIColor(red: CGFloat(217) / 255.0, green: CGFloat(217) / 255.0, blue: CGFloat(217) / 255.0, alpha: 1.0 )
        tableView.tableFooterView = UIView()
        //beginParsing()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func example()
    {
        equationName = "Fraction"
        equation = "<math>" +
        "<apply>" +
        "<eq/>" +
        "<ci>a</ci>" +
        "<apply>" +
        "<divide/>" +
        "<ci>b</ci>" +
        "<ci>c</ci>" +
        "</apply>" +
        "</apply>" +
        "</math>"
        webDesription = "work"
        add()
    }
    
    func example2()
    {
        equationName = "Percentage"
        equation = "<math>" +
        "<apply>" +
        "<eq/>" +
        "<ci>a</ci>" +
        "<apply>" +
        "<times/>" +
        "<apply>" +
		"<divide/>" +
		"<ci>b</ci>" +
		"<ci>c</ci>" +
        "</apply>" +
        "<cn>100</cn>" +
        "</apply>" +
        "</apply>" +
        "</math>"
        webDesription = "<p>A percentage is a ratio or fraction a/b which is expressed as a fraction of 100 i.e a/b x 100.  It is denoted by the % sign.</p>" +
        "<p>If different fractions of a whole are expressed as percentages, we can compare them very easily.</p>" +
        "<p>For example, lets compare 5/8 and 6/11. Which is the largest number? It's not all obvious. But if we express them as a percentage, 5/8 is 62.5% and 6/11 is 54.5%, so 5/8 is the largest number. This is just one use for percentages; there are many more.</p>"
        add()
    }
    
    func example3()
    {
        equationName = "Pressure"
        equation =
        "<math>" +
        "<apply>" +
        "<eq/>" +
        "<ci>P</ci>" +
        "<apply>" +
        "<divide/>" +
        "<ci>F</ci>" +
        "<ci>A</ci>" +
        "</apply>" +
        "</apply>" +
        "</math>"
        webDesription = "<p>Pressure is the effect of a force applied to a surface and is described as the amount of force which acts over a unit area. The unit of pressure is the Newton per square meter and is called the Pascal.</p><p>The pressure which results from the air above us is 101,000 Pa. This means that 1 square meter at sea level has a force 101,000 Newtons acting on it! We don't feel it however because the pressure on both sides of things is the same, so they are balanced and cancel out. If however we suck the air out of a plastic bottle or a can, the forces are no longer balanced and the weight of the air outside acts on it and soon crushes the container.</p>"
        add()
    }
    
    func example4()
    {
        equationName = "Weight"
        equation =
        "<math>" +
        "<apply>" +
		"<eq/>" +
		"<ci>W</ci>" +
		"<apply>" +
        "<times/>" +
        "<ci>m</ci>" +
        "<ci>g</ci>" +
        "</apply>" +
        "</apply>" +
        "</math>"
        webDesription = "<p>Weight is the force exerted on an object from a gravitational field.</p><p>The earth is surrounded by a gravitational field which pulls every mass towards it. The size of this gravitational field is called the gravitational field strength or g, and has units of Newtons/kilogram. Earth's value of g is 9.81 N/kg.</p><p>Other planets have different values of gravitational field strength.</p><p>The important thing to remember is that the mass of an object in kilograms never changes no matter where you are in the universe and tells us the amount of matter that is in the object. The weight of an object is the force that results from the gravitational field. This does change because it depends on the strength of the gravitational field.</p>"
        add()
    }
    
    func example5()
    {
        equationName = "Velocity"
        equation =
        "<math>" +
        "<apply>" +
		"<eq/>" +
		"<ci>v</ci>" +
		"<apply>" +
        "<divide/>" +
        "<ci>d</ci>" +
        "<ci>t</ci>" +
		"</apply>" +
        "</apply>" +
        "</math>"
        webDesription = "<p>Velocity tells us how fast something travels and is calculated using two measurements; the distance travelled and the time taken to travel that distance.</p><p>Velocity has dimensions of metres/second (m/s) but many other units are in common usage such as km/hour or miles/hour. You can think of velocity as the rate of change of distance.</p><p>Example: If a car travels 15 metres in 3 seconds, its velocity would be 5 m/s.</p><p>People often use the terms velocity and speed interchangeably when talking about how fast things travel. In physics, these terms have different and quite precise meanings. To understand the difference we need to understand the difference between vector and scalar quantities. A vector quantity has both magnitude and direction (acceleration and force are other examples of vector quantities i.e. they have a value and always have a direction associated with them). A scalar quantity has magnitude only; direction is not applicable (mass and energy are examples of scalar quantities).</p><p>Strictly speaking, velocity is a vector quantity and speed is a scalar quantity.</p><p>When we describe a velocity we should really identify how fast an object travels and the direction it's travelling in. For example an aeroplane travels west at 500km/hour.</p><p>When we talk about speed, we just need to identify how fast the object is travelling; direction is not required.</p>"
        add()
    }
    
    func example6()
    {
        equationName = "Proportion"
        equation =
        "<math>" +
        "<apply>" +
        "<eq/>" +
        "<apply>" +
        "<divide/>" +
        "<ci>a</ci>" +
        "<ci>b</ci>" +
        "</apply>" +
        "<apply>" +
        "<divide/>" +
        "<ci>c</ci>" +
        "<ci>d</ci>" +
        "</apply>" +
        "</apply>" +
        "</math>"
        webDesription = "<p>Proportion is the name we give to 2 ratios which are equal to each other. If the two ratios are equal then the variables that make them up are directly proportional to each other. This has an important outcome, if we know the value of any 3 of quantities, we can calculate the 4th unknown quantity.</p>" +
        "<p>For example, you can use proportion to calculate the answer to the following question. In a sample of salt water, we know that 25g of salt are dissolved in 2 litres of water. How many litres of solution would we need to have 5 g of salt? This is a typical example of proportion and where we use two ratios. Solve this by using the proportion:</p> " +
        "<p>x/2 = 5/25</p>" +
        "<p>x=10/25 = 0.4 litres</p>"
        add()
    }
    
    func add()
    {
        var AddedData = Data()
        AddedData.setName(equationName)
        webData = "<?xml version='1.0' encoding='UTF-8'?> <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.1 plus MathML 2.0//EN' 'http://www.w3.org/Math/DTD/mathml2/xhtml-math11-f.dtd'> <html xmlns='http://www.w3.org/1999/xhtml'> <head> <script type='text/javascript' async src='https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=MML_CHTML'> </script> <script type='text/x-mathjax-config'> MathJax.Hub.Config ({ MathML: {extensions: ['content-mathml.js']}});</script> </head> <body style='background-color: transparent;'>" + equation + "</body></html>"
        println(equation)
        AddedData.setWebData(webData)
        AddedData.setDescription(webDesription)
        data.append(AddedData)
    }
    
    func clear()
    {
        webData = ""
        webDesription = ""
        equationName = ""
        equation = ""
        variables = ""
    }
    
    /*func beginReading()
    {
    }
    
    func beginParsing()
    {
        let path = NSBundle.mainBundle().pathForResource("Equations", ofType: "xml")
        if path != nil {
            println("yep")
            parser = NSXMLParser(contentsOfURL: NSURL(fileURLWithPath: path!))!
            parser.delegate = self
            var success:Bool = parser.parse()
            if (success)
            {
                println("Success")
            }
            else
            {
                println("Failed")
            }
        } else {
            println("nope")
        }

    }
    
    func parser(parser: NSXMLParser!, didStartElement elementName: String!, namespaceURI: String!, qualifiedName qName: String!, attributes attributeDict: [NSObject : AnyObject]!) {
        if elementName == "equation"
        {
            endedEquation = false
            equationName = attributeDict["name"] as String
        }
        if elementName == "description"
        {
            endedDescription = false
        }
        if elementName == "variables"
        {
            endedVariable = false
        }
        if elementName == "math"
        {
            endedMath = false
        }
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String?) {
        if endedEquation == false {
            full = full + string!
        }
        if endedDescription == false
        {
            webDesription = webDesription + string!
        }
        if endedVariable == false
        {
            variables = variables + string!
        }
        if endedMath == false
        {
            equation = equation + string!
        }
        
        println(string)
    }
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "equation" {
            endedEquation = true
            println(full)
            add()
            clear()
        }
        if elementName == "description"
        {
            endedDescription = true
        }
        if elementName == "math"
        {
            endedMath = true
        }
    }
    
    func parser(parser: NSXMLParser, parseErrorOccurred parseError: NSError) {
        NSLog("failure error: %@", parseError)
    }*/
    
    func loadSampleData()
    {
        example()
        example2()
        example3()
        example4()
        example5()
        example6()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("WebTableViewCell", forIndexPath: indexPath) as WebTableViewCell
        // Configure the cell...
        let dataScroll = data[indexPath.row]
        var myPath: Path!
        
        cell.MainLabel.text = dataScroll.getName()
        cell.MainWebView.loadHTMLString(dataScroll.getWebData(), baseURL: nil)
        cell.MainWebView.backgroundColor = UIColor.clearColor()
        var tableTap = UITapGestureRecognizer(target: self, action: "tableCellTapped:")
        //var tablePan = UIPanGestureRecognizer(target: self, action: "tableCellDragged:")
        cell.addGestureRecognizer(tableTap)
       // cell.addGestureRecognizer(tablePan)
        cell.tag = 1000 + indexPath.row
        dataScroll.setTag(cell.tag)
        //cell.backgroundColor = UIColor(red: CGFloat(242) / 255.0, green: CGFloat(242) / 255.0, blue: CGFloat(242) / 255.0, alpha: 1.0 )
        cell.backgroundColor = UIColor.clearColor()
        cell.separatorInset =  UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
        cell.layoutMargins = UIEdgeInsetsZero
        if indexPath.row % 2 != 0
        {
            cell.backgroundColor = UIColor(red: CGFloat(230) / 255.0, green: CGFloat(230) / 255.0, blue: CGFloat(255) / 255.0, alpha: 1.0)
        }
        else{
            cell.backgroundColor = UIColor.whiteColor()
        }
        //gestureArray.append(PanClass(tag: cell.tag, pan: tablePan))
        paths.append(Path(tag: cell.tag, path: indexPath))
        return cell
    }
    
    func tableCellTapped(sender: UITapGestureRecognizer)
    {
        if let vc = self.parentViewController as? ViewController
        {
            println("Dynamic view Created")
            for index in 0 ... (data.count - 1)
            {
                println("in")
                println(data[index].getTag())
                if data[index].getTag() == sender.view!.tag
                {
                    println("Out")
                    vc.createDynam(data[index])
                }
            }
        }
    }
    
    func tableCellDragged(sender: UIPanGestureRecognizer)
    {
        var Tag = sender.view!.tag
        var panEnd: UIPanGestureRecognizer!
        
        if sender.state == UIGestureRecognizerState.Began
        {
            println("1")
            var positionx = TableView.viewWithTag(Tag)!.center.x
            var positiony = TableView.viewWithTag(Tag)!.center.y
            setPosition(positionx, y: positiony)
            self.view.bringSubviewToFront(TableView.viewWithTag(Tag)!)
            
        }
        
        if sender.state == UIGestureRecognizerState.Changed
        {
            println("2")
            if let vc = self.parentViewController as? ViewController
            {
                println(TableView.viewWithTag(Tag)?.center)
                //createGhostView(sender.view!)
                //TableView.viewWithTag(Tag)?.center = sender.locationInView(TableView)
                /*if TableView.viewWithTag(Tag)?.center.x >= 220
                {
                    println("OOPS")
                    var limit = paths.count - 1
                    for index in 0 ... limit
                    {
                        if paths[index].getTag() == Tag
                        {
                            vc.createWebCell(paths[index].getPath(), pos: Position)
                        }
                    }
                }*/
            }
        }
        
        if sender.state == UIGestureRecognizerState.Ended
        {
            println("3")
            TableView.viewWithTag(Tag)?.center = Position
        }
    }
    
    func setPosition(x: CGFloat, y: CGFloat)
    {
        Position = CGPointMake(x, y)
    }
    
    /*func createGhostView(sender: UIView)
    {
        println("hey")
        var cell: WebTableViewCell!
        for index in 0...data.count - 1
        {
            if data[index].Tag == sender.tag
            {
                cell.MainLabel.text = data[index].getName()
                cell.MainWebView.loadHTMLString(data[index].getWebData(), baseURL: nil)
                cell.center = CGPointMake(150.0, 150.0)
                if let vc = self.parentViewController as? ViewController
                {
                    vc.view.addSubview(cell)
                }
                break
            }
        }

        
    }*/
    
}
