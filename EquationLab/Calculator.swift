//
//  Calculator.swift
//  EquationLab
//
//  Created by Bogdanov, Ivaylo on 26/03/2016.
//  Copyright (c) 2016 Giannarakis, Paul. All rights reserved.
//

import UIKit
import Foundation

class Calculator: UIView {
    
    // MARK:- keyboard initialization
    
    @IBOutlet weak var display: UILabel!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var button7: UIButton!
    @IBOutlet weak var button8: UIButton!
    @IBOutlet weak var button9: UIButton!
    @IBOutlet weak var backspace: UIButton!
    @IBOutlet weak var AC: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var button5: UIButton!
    @IBOutlet weak var button6: UIButton!
    @IBOutlet weak var multiply: UIButton!
    @IBOutlet weak var divide: UIButton!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var plus: UIButton!
    @IBOutlet weak var minus: UIButton!
    @IBOutlet weak var button0: UIButton!
    @IBOutlet weak var dot: UIButton!
    @IBOutlet weak var negpos: UIButton!
    @IBOutlet weak var equals: UIButton!
    @IBOutlet weak var sin: UIButton!
    @IBOutlet weak var cos: UIButton!
    @IBOutlet weak var tan: UIButton!
    @IBOutlet weak var pi: UIButton!
    @IBOutlet weak var divx: UIButton!
    @IBOutlet weak var sinmin: UIButton!
    @IBOutlet weak var cosmin: UIButton!
    @IBOutlet weak var tanmin: UIButton!
    @IBOutlet weak var xpowtwo: UIButton!
    @IBOutlet weak var square: UIButton!
    @IBOutlet weak var xten: UIButton!
    @IBOutlet weak var loge: UIButton!
    @IBOutlet weak var ex: UIButton!
    @IBOutlet weak var xy: UIButton!
    @IBOutlet weak var ysquarex: UIButton!
    
    var operandStack = [Double]()
    var operatorSymbol: String?
    var userIsInTheMiddleOfTypingANumber = false
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeSubviews()
        buttonborders()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeSubviews()
        buttonborders()
    }
    
    func initializeSubviews() {
        let xibFileName = "Calculator" // xib extention not included
        let view = NSBundle.mainBundle().loadNibNamed(xibFileName, owner: self, options: nil)[0] as UIView
        self.addSubview(view)
        view.frame = self.bounds
    }
    
    func buttonborders()
    {
        button7.layer.borderWidth = 0.5
        button8.layer.borderWidth = 0.5
        button9.layer.borderWidth = 0.5
        backspace.layer.borderWidth = 0.5
        AC.layer.borderWidth = 0.5
        button4.layer.borderWidth = 0.5
        button5.layer.borderWidth = 0.5
        button6.layer.borderWidth = 0.5
        multiply.layer.borderWidth = 0.5
        divide.layer.borderWidth = 0.5
        button1.layer.borderWidth = 0.5
        button2.layer.borderWidth = 0.5
        button3.layer.borderWidth = 0.5
        plus.layer.borderWidth = 0.5
        minus.layer.borderWidth = 0.5
        button0.layer.borderWidth = 0.5
        dot.layer.borderWidth = 0.5
        negpos.layer.borderWidth = 0.5
        sin.layer.borderWidth = 0.5
        cos.layer.borderWidth = 0.5
        tan.layer.borderWidth = 0.5
        pi.layer.borderWidth = 0.5
        divx.layer.borderWidth = 0.5
        sinmin.layer.borderWidth = 0.5
        cosmin.layer.borderWidth = 0.5
        tanmin.layer.borderWidth = 0.5
        xpowtwo.layer.borderWidth = 0.5
        square.layer.borderWidth = 0.5
        xten.layer.borderWidth = 0.5
        loge.layer.borderWidth = 0.5
        ex.layer.borderWidth = 0.5
        xy.layer.borderWidth = 0.5
        ysquarex.layer.borderWidth = 0.5
    }
    
    
    // not used for user typing, just calculations
    var displayValue: Double {
        get {
            return NSNumberFormatter().numberFromString(display.text!)!.doubleValue
        }
        set {
            // remove ".0" from display
            if newValue == floor(newValue) {
                display.text = "\(Int(newValue))"
            } else {
                display.text = "\(newValue)"
            }
            
            userIsInTheMiddleOfTypingANumber = false
        }
    }
    
    
    @IBAction func appenddigit(sender: UIButton) {
        
        let digit = sender.currentTitle!
        if userIsInTheMiddleOfTypingANumber {
            if (digit == ".") && (display.text!.rangeOfString(".") != nil) { return }
            display.text = display.text! + digit
        } else {
            if digit == "." {
                display.text = "0."
            } else {
                display.text = digit
            }
            userIsInTheMiddleOfTypingANumber = true
        }
    }

    
    @IBAction func operate(sender: UIButton) {
        
        userIsInTheMiddleOfTypingANumber = false
        operatorSymbol = sender.currentTitle!
        operandStack.append(displayValue)
    }
    
    @IBAction func Clear(sender: UIButton) {
        
        operandStack.removeAll()
        userIsInTheMiddleOfTypingANumber = false
        operatorSymbol = nil
        displayValue = 0
        
    }
    
    @IBAction func undo(sender: UIButton) {
        if userIsInTheMiddleOfTypingANumber {
            let displaytext = display.text!
            if countElements(displaytext) > 1 {
                display.text = dropLast(displaytext)
            }
            else{
                display.text = "0"
                userIsInTheMiddleOfTypingANumber = false
            }
            
        }
        
    }
    
    func performOperation( operation: (Double, Double) -> Double)  {
        displayValue = operation(operandStack.removeLast(), operandStack.removeLast())
        operandStack.append(displayValue)
    }
    
    func performOperation( operation: Double -> Double)  {
        // if operandStack.count >=1 {
        displayValue = operation(operandStack.removeLast())
        operandStack.append(displayValue)
        // }
    }
    
    @IBAction func equals(sender: UIButton) {
        
        if operatorSymbol != nil {
            userIsInTheMiddleOfTypingANumber = false
            operandStack.append(displayValue)
            
            switch operatorSymbol! {
            case "+": performOperation() { $0 + $1 }
            case "−": performOperation() { $1 - $0 }
            case "×": performOperation() { $0 * $1 }
            case "÷": performOperation() { $1 / $0 }
                
            default: break
            }
            
            print("\(operandStack) " + operatorSymbol!)
        }
        
        
        
    }
    @IBAction func squareroot(sender: UIButton) {
        if operatorSymbol != nil {
            userIsInTheMiddleOfTypingANumber = false
            //operandStack.append(displayValue)
            
            switch operatorSymbol! {
            case "√": performOperation() {sqrt($0)}
            default: break
            }
            
            print("\(operandStack) " + operatorSymbol!)
        }
        
        
    }
    
    @IBAction func minus(sender: UIButton) {
        
        if operatorSymbol != nil {
            userIsInTheMiddleOfTypingANumber = false
            operandStack.append(displayValue)
            
            switch operatorSymbol! {
                
            case "±": performOperation() { -($0)}
            default: break
            }
            
            print("\(operandStack) " + operatorSymbol!)
        }
        
    }
  
    @IBAction func Close(sender: UIButton) {
        
       
           self.removeFromSuperview()
        
    }
}
