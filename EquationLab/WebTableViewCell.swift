//
//  WebTableViewCell.swift
//  EquationLab
//
//  Created by Giannarakis, Paul on 06/04/2016.
//  Copyright (c) 2016 Giannarakis, Paul. All rights reserved.
//

import UIKit

class WebTableViewCell: UITableViewCell {
    
    //MARK: Properties
    
    @IBOutlet weak var MainLabel: UILabel!
    @IBOutlet weak var MainWebView: UIWebView!
    
    //@IBOutlet weak var starBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setMe()
        //var tableTap = UITapGestureRecognizer(target: self, action: "setGestureTap")
        //self.addGestureRecognizer(tableTap)
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setMe()
    {
        MainWebView.scrollView.scrollEnabled = false
        MainWebView.scrollView.bounces = false
        MainWebView.backgroundColor = UIColor.clearColor()
        MainWebView.opaque = false
    }
    
}
