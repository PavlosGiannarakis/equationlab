//
//  Path.swift
//  EquationLab
//
//  Created by Giannarakis, Paul on 17/04/2016.
//  Copyright (c) 2016 Giannarakis, Paul. All rights reserved.
//

import UIKit

class Path {
    
    private var Tag: Int!
    private var Path1: NSIndexPath!
    
    init(tag: Int, path: NSIndexPath )
    {
        addPath(path)
        addTag(tag)
    }
    
    func addPath(addP: NSIndexPath)
    {
        self.Path1 = addP
    }
    
    func addTag(addT: Int)
    {
        self.Tag = addT
    }
    
    func getPath() -> NSIndexPath
    {
        return Path1
    }
    func getTag() ->Int
    {
        return Tag
    }
}
