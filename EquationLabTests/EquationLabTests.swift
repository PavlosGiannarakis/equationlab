//
//  EquationLabTests.swift
//  EquationLabTests
//
//  Created by Giannarakis, Paul on 12/02/2016.
//  Copyright (c) 2016 Giannarakis, Paul. All rights reserved.
//

import UIKit
import XCTest

class EquationLabTests: XCTestCase {
    var testerVc : ViewController!
    var testWebtable : WebTableViewController!
    
    override func setUp() {
        super.setUp()
        var storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: NSBundle(forClass: self.dynamicType))
        testerVc = storyboard.instantiateViewControllerWithIdentifier("ViewController1") as ViewController
        testWebtable = storyboard.instantiateViewControllerWithIdentifier("WebTableVC") as WebTableViewController
        testerVc.loadView()
        testWebtable.loadView()
        testWebtable.loadSampleData()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDynamicViewVC1() {
        var name = "Testing"
        var description = "For learning purposes"
        var webData = "some equation"
        
        var data2 = Data()
        data2.setName(name)
        data2.setDescription(description)
        data2.setWebData(webData)
        var count = testerVc.Main_view.subviews.count - 1

        for i in 100 ... 103
        {
            testerVc.createView(i, data: data2)
            if i == 100
            {
                testerVc.DataOf100 = data2
            }
            else if i == 101
            {
                testerVc.DataOf101 = data2
            }
            else if i == 102
            {
                testerVc.DataOf102 = data2
            }
            else if i == 103
            {
                testerVc.DataOf103 = data2
            }
        }
        
        XCTAssertNotNil(testerVc.Main_view.viewWithTag(100)?.isDescendantOfView(testerVc.Main_view), "Pass tag 100")
        XCTAssertNotNil(testerVc.Main_view.viewWithTag(101)?.isDescendantOfView(testerVc.Main_view), "Pass tag 101")
        XCTAssertNotNil(testerVc.Main_view.viewWithTag(102)?.isDescendantOfView(testerVc.Main_view), "Pass tag 102")
        XCTAssertNotNil(testerVc.Main_view.viewWithTag(103)?.isDescendantOfView(testerVc.Main_view), "Pass tag 103")
        
        testerVc.gotFocus(102) // gives focus
        
        XCTAssertFalse(testerVc.Equations_view.hidden, "Pass visibility") 
        
        XCTAssertEqual(testerVc.Main_view.viewWithTag(102)!.backgroundColor! , UIColor.whiteColor(), "Pass Name 100")
        
        XCTAssertEqual(testerVc.DataOf100.getName() , data2.getName(), "Pass Name 100")
        XCTAssertEqual(testerVc.DataOf101.getName() , data2.getName(), "Pass Name 101")
        XCTAssertEqual(testerVc.DataOf102.getName() , data2.getName(), "Pass Name 102")
        XCTAssertEqual(testerVc.DataOf103.getName() , data2.getName(), "Pass Name 103")
        
        XCTAssertEqual(testerVc.DataOf100.getDescription() , data2.getDescription(), "Pass description 100")
        XCTAssertEqual(testerVc.DataOf101.getDescription() , data2.getDescription(), "Pass description 101")
        XCTAssertEqual(testerVc.DataOf102.getDescription() , data2.getDescription(), "Pass description 102")
        XCTAssertEqual(testerVc.DataOf103.getDescription() , data2.getDescription(), "Pass description 103")
        
        XCTAssertEqual(testerVc.DataOf100.getWebData() , data2.getWebData(), "Pass WebData 100")
        XCTAssertEqual(testerVc.DataOf101.getWebData() , data2.getWebData(), "Pass WebData 101")
        XCTAssertEqual(testerVc.DataOf102.getWebData() , data2.getWebData(), "Pass WebData 102")
        XCTAssertEqual(testerVc.DataOf103.getWebData() , data2.getWebData(), "Pass WebData 103")
        
        testerVc.controlDynam(6, SuperVTagBtn: 100) // Removes view
        XCTAssertNotNil(testerVc.Main_view.viewWithTag(100)?.isDescendantOfView(testerVc.Main_view), "Pass tag 100")
        

    }
    
    /*func testTapCell()
    {
        var count: Int = testWebtable.tableView(testWebtable.tableView, numberOfRowsInSection: 0)
        let cell = testWebtable.tableView(testWebtable.tableView, cellForRowAtIndexPath: NSIndexPath(forRow: 0, inSection: 0)) as WebTableViewCell
        XCTAssertNotNil(testerVc.Main_view.viewWithTag(100)?.isDescendantOfView(testerVc.Main_view), "Pass tag 100")
    }*/
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
}
